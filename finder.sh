#!/bin/bash


NEWLOC=`curl -L -I "http://www.bombich.com/software/download_ccc.php?v=latest" -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36' 2>/dev/null | grep location | sed 's/location: //' | tail -1 | tr -d '\r'`

if [ "x${NEWLOC}" != "x" ]; then
	echo "${NEWLOC}"
fi
